package com.disguise;

import com.disguise.protocol.TinyProtocol;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class ExamplePlugin extends JavaPlugin {

    private static ExamplePlugin instance;

    public static ExamplePlugin getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        new TinyProtocol(this);
        DisguiseUtil.register();
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("disguise")) {
            if (sender instanceof Player) {
                Player p = (Player) sender;
                DisguiseUtil d  = DisguiseUtil.instanceOf(p);
                if(d == null || !d.isDisguised()) {
                    DisguiseUtil disguise = new DisguiseUtil(EntityType.POLAR_BEAR, p);
                    disguise.disguisePlayer(Bukkit.getOnlinePlayers());
                }  else
                    d.removeDisguise();
            }
        }
        return false;
    }

}
